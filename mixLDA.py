""" Use both text and image for topic extraction using LDA"""

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import MiniBatchKMeans, AgglomerativeClustering
from sklearn.decomposition import LatentDirichletAllocation
from lda_text import TextLda
import lda

# data_path = os.path.join('MemeSample', 'data.csv')
data_path = os.path.join('MemeSample', 'data.csv')
textLda = TextLda(data_path, encoding='cp1252', use_bigrams=True)
# Use id from data to get image so that we can reference using same index
images = []
# ids of memes that doesn't belong in any category
other_id = [26, 73, 334, 178, 180, 275, 342, 95, 128]
for img_id in textLda.data['Id']:
    if img_id in other_id:
        continue
    img_path = os.path.join('memes', str(img_id) + '.png')
    image = cv2.imread(img_path)
    image = cv2.resize(image, (256, 256))
    images.append(image)

N = len(images)
print('number of data:', N)
# getting random data to check the image and text
rand_index = np.random.choice(range(N))
print(textLda.preprocessed_data[rand_index])
plt.imshow(images[rand_index])
plt.show()
train_x = np.array(images)
dictionary = textLda.dictionary
text_dictionary_size = len(dictionary)
text_feature_vector = np.zeros((N, text_dictionary_size))
# build a document-term matrix as feature vector
for doc_idx, doc in enumerate(textLda.corpus):
    for key, word_count in doc:
        text_feature_vector[doc_idx][key] = 4 * word_count    # providing 4 times the weight for text feature
print(text_feature_vector.shape)

# build BOVW (Bag of visual words) for images
cnn_model = lda.get_vgg_model()
features, num_kps, image_patches = lda.get_cnn_features(cnn_model, train_x, 30)
print("shape of featureset: ", features.shape)
no_of_topics = 6
k = 200     # number of visual words
print("number of visual words: ", k)
kmeans = MiniBatchKMeans(n_clusters=k,
                         init='k-means++',
                         batch_size=500,
                         random_state=0,
                         verbose=0)
kmeans.fit(features)
k_labels = kmeans.labels_
visual_word_clusters = lda.get_cluster_from_labels(k_labels)
k_centers = kmeans.cluster_centers_
td_mat = lda.vector_quantize(k, N, num_kps, k_labels)
visual_feature_vector = td_mat.T
print(visual_feature_vector.shape)

# combine visual feature and textual feature to pass to LDA
feature_vector = np.append(visual_feature_vector, text_feature_vector, axis=1)
print(feature_vector.shape)
max_iterations = 100
print('lda max_iterations: ', max_iterations)
for n_topics in [no_of_topics]:
    print("number of topics: ", n_topics)
    m_predictions = []
    topics_i = []  # topic clusters for ith iteration
    ith_topic_matrix = []
    for i in range(max_iterations):
        lda_model = LatentDirichletAllocation(n_components=n_topics, learning_method='online', batch_size=512)
        lda_model.fit(feature_vector)
        topic_matrix = lda_model.components_ / lda_model.components_.sum(axis=1)[:, np.newaxis]  # topics X terms
        ith_topic_matrix.append(topic_matrix)

        train_topic_matrix = lda_model.transform(feature_vector)

        topics_train, train_predictions = lda.get_topics(train_topic_matrix)
        topics_i.append(topics_train)
        m_predictions.append(train_predictions)

    m_predictions = np.array(m_predictions)  # iteration * image
    n_predictions = np.transpose(m_predictions)  # image * iteration
    print(n_predictions.shape)
    n = n_predictions.shape[0]
    distance_matrix = np.zeros((n, n))
    # for each pair of images
    cache = {}
    for i, predi in enumerate(n_predictions):
        for j, predj in enumerate(n_predictions):
            key = frozenset([i, j])
            if cache.get(key):
                distance_matrix[i][j] = cache.get(key)
            else:
                count = sum([ii == jj for ii, jj in zip(predi, predj)]) / max_iterations
                # count the fraction of times they belong to the same topic
                distance_matrix[i][j] = 1 - count  # dist = 1 - similarity
                cache[key] = 1 - count

    print(distance_matrix)
    # for row in distance_matrix:
    #     print(row)
    clustering = AgglomerativeClustering(n_clusters=no_of_topics, affinity='precomputed', linkage='average')
    predictions = clustering.fit_predict(distance_matrix)
    textLda.print_score(predictions)
    final_clusters = lda.get_cluster_from_labels(predictions)
    # print(ith_topic_matrix)
    cluster_def = {}
    # find the closest topic to clusters for interpretability
    for cluster_idx, img_indexes in final_clusters.items():
        max_count = -1
        i = -1
        topic_i = -1
        for iteration_number, topic_clusters in enumerate(topics_i):
            # print('iteration: ', iteration_number)
            for topic_idx, image_indexes_topic in topic_clusters.items():
                # print('images in topic: ', image_indexes_topic)
                count = sum([i in image_indexes_topic for i in img_indexes])
                # print('count: ', count)
                if count > max_count:
                    max_count = count
                    i = iteration_number
                    topic_i = topic_idx
        cluster_def[cluster_idx] = ith_topic_matrix[i][topic_i]

    cmat = np.zeros((n_topics, feature_vector.shape[1]))
    for c_index, defn in cluster_def.items():
        cmat[c_index] = defn
    plt.matshow(cmat, cmap='gray')
    plt.show()

    # getting top 10 words for a topic with their probabilities
    for t_idx, word_row in enumerate(cmat):
        print('topic: ' + str(t_idx))
        top10 = np.argsort(word_row)[-10:]
        cluster_imgs = [train_x[i] for i in final_clusters.get(t_idx)]
        lda.plot_images(4, 4, cluster_imgs[:16], str(t_idx), save=True)
        for top_i in top10[::-1]:
            if top_i < k:    # visual word
                print("visual word " + str(top_i) + ": " + str(word_row[top_i]))
                word_patches = [image_patches[i] for i in visual_word_clusters.get(top_i)]
                lda.plot_images(4, 4, word_patches[:16], 'word #' + str(top_i), save=True)
                lda.visualize_cnn(cnn_model, word_patches[0], [1, 16], 'word #' + str(top_i))
            else:
                print(dictionary.get(top_i-k) + ": " + str(word_row[top_i]))
        # print the texts
        texts = [textLda.data.iloc[i].Text for i in final_clusters.get(t_idx)]
        for text in texts[:10]:
            print(text)
