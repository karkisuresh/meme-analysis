import os
import glob
import time
import keras
import warnings
import numpy as np
import matplotlib.pyplot as plt
import ml_metrics as metrics
from sklearn.model_selection import train_test_split
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.metrics import *
from sklearn import datasets
from sklearn.metrics.pairwise import cosine_similarity
from keras.datasets import cifar10, mnist
from keras.applications.vgg16 import VGG16
from keras.applications.resnet50 import ResNet50
from keras import Model
from collections import Counter

import cv2
cv2.setUseOptimized(True)
cv2.setNumThreads(4)

warnings.filterwarnings("ignore", category=FutureWarning)

# np.random.seed(200)


def plot_images(n_rows, n_cols, images, title, is_matrix=False, save=False):
    """"
    Method that plots images
    :param n_rows: number of rows
    :param n_cols: number of columns
    :param images: array of images to plot
    :param title: title of plot
    :param is_matrix: if plot is of matrix or image
    :param save: save the figure or not
    :return: None
    """
    f = plt.figure()
    f.suptitle(title)
    for index, image in enumerate(images):
        plt.subplot(n_rows, n_cols, index + 1)
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        if is_matrix:
            plt.matshow(images[index])
        else:
            plt.imshow(cv2.cvtColor(images[index], cv2.COLOR_BGR2RGB))
    plt.show()
    if save:
        f.savefig(os.path.join('viz', title), dpi=f.dpi)
    plt.close(f)


def get_sift_features(images, n):
    """
    Computes SIFT features. this works only with opencv-contrib-python==3.4.2.16 as the upper versions have patented
    the algorithm and needs to be compiled separate.
    :param images: list of images
    :param n: number of images
    :return: list of descriptors and number of keypoints
    """
    print("getting sift features")
    n_keypoints = np.zeros(n)  # store for number of keypoints for each image
    sift = cv2.xfeatures2d.SIFT_create()
    for idx in range(n):
        img = images[idx]
        # img = cv2.cvtColor(images[idx], cv2.COLOR_BGR2GRAY)
        # compute keypoints and their descriptors
        kp, desc = sift.detectAndCompute(img, None)
        n_keypoints[idx] = len(kp)
        if len(kp):
            if idx == 0:
                img_kp = cv2.drawKeypoints(img, kp, color=(0, 255, 0), outImage=np.array([]))
                #plt.imshow(img_kp)
                #plt.show()
                descriptors = desc
            else:
                descriptors = np.vstack((descriptors, desc))
    return descriptors, n_keypoints


def vector_quantize(dictionary_size, n, n_keypoints, labels):
    """
    Performs vector quantization
    :param dictionary_size: no of clusters (size of dictionary)
    :param n: number of images
    :param n_keypoints: list of number of keypoints for each image
    :param labels: labels of descriptors
    :return: term document matrix
    """
    mat = np.zeros((dictionary_size, n))
    i = 0
    for index in range(n):
        if index == 0:
            n_kps = int(n_keypoints[index])
            mat[:, index], bins = np.histogram(labels[0:n_kps], bins=range(dictionary_size + 1))
        else:
            i = np.int(i + n_keypoints[index - 1])
            j = np.int(i + n_keypoints[index])
            mat[:, index], bins = np.histogram(labels[i:j], bins=range(dictionary_size + 1))
    return mat


# loading event data
def get_uuic_data():
    print("getting UUIC data")
    x = []
    y = []
    label_encoder = {'badminton': 0, 'bocce': 1, 'croquet': 2, 'polo': 3, 'RockClimbing': 4, 'rowing': 5, 'sailing': 6,
                     'snowboarding': 7}

    for label in label_encoder:
        counter = 0
        path = os.path.join('event_img', os.path.join(label, '*.jpg'))
        for image_path in glob.glob(path):
            # print(imge)
            imag = cv2.imread(image_path)
            imag = cv2.resize(imag, (256, 256))
            x.append(imag)
            y.append(label_encoder.get(label))
            counter += 1
            if counter == 500:
                break
    return x, y


# loading memes data
def get_memes_data():
    x = []
    y = []
    path = os.path.join('MemeSample', '*.png')
    for image_path in glob.glob(path):
        # print(image_path)
        imag = cv2.imread(image_path)
        # imag = cv2.resize(imag, (256, 256))
        x.append(imag)
        y.append(1)
    return x, y


def get_cnn_features(model, images_list, number_of_regions):
    selective_search = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()
    print("number of regions", number_of_regions)
    im_out = images_list[0].copy()
    feature_set = []
    no_of_patches = []
    all_regions = []
    for img_idx, image in enumerate(images_list):
        if img_idx % 100 == 0:
            print("image no: ", img_idx + 1)
        patches = []
        selective_search.setBaseImage(image)
        selective_search.switchToSelectiveSearchFast()
        regions = selective_search.process()
        for rect in regions[:number_of_regions]:
            xi, yi, wi, hi = rect
            patch = image[yi:yi+hi, xi:xi+wi]
            all_regions.append(patch)
            patch_resized = cv2.resize(patch, (224, 224), interpolation=cv2.INTER_AREA)
            patches.append(patch_resized)
            if img_idx == 0:
                cv2.rectangle(im_out, (xi, yi), (xi+wi, yi+hi), (0, 255, 0), 1, cv2.LINE_AA)
        if img_idx == 0:
            plt.imshow(cv2.cvtColor(im_out, cv2.COLOR_BGR2RGB))
            plt.show()
        patches = np.array(patches)
        no_of_patches.append(patches.shape[0])
        # print("shape of image_patches: ", patches.shape)
        feature_set.extend(model.predict(patches))
    feature_set = np.array(feature_set)
    return feature_set, no_of_patches, all_regions


def get_topics(topic_mat):
    labels = []
    for img_indx, row in enumerate(topic_mat):
        topic_indx = int(np.argmax(row))
        labels.append(topic_indx)
        # print(topic_indx)
        topic_prob_for_img = row[topic_indx]
    topics = get_cluster_from_labels(labels)
    return topics, labels


def get_cluster_from_labels(labels):
    clusters = {}
    for idx, label in enumerate(labels):
        if not clusters.get(label):
            clusters[label] = [idx]
        else:
            (clusters[label]).append(idx)
    return clusters


def get_vgg_model():
    print("loading VGG model")
    vgg = VGG16(weights='imagenet', include_top=True)
    # removing classification layer
    vgg.layers.pop()
    # Freeze the layers
    for layer in vgg.layers:
        layer.trainable = False
    output = vgg.layers[-1]
    model = Model(vgg.inputs, output.output)
    model.summary()
    return model


def get_resnet_model():
    print("loading resnet50 model")
    resnet = ResNet50(weights='imagenet', include_top=True)
    # removing classification layer
    resnet.layers.pop()
    # Freeze the layers
    for layer in resnet.layers:
        layer.trainable = False
    output = resnet.layers[-1]
    model = Model(resnet.inputs, output.output)
    model.summary()
    return model


def visualize_cnn(model, image, act_indexes, title):
    outputs = [layer.output for layer in model.layers][1:]
    act_model = Model(inputs=model.input, outputs=outputs)
    x = np.array([cv2.resize(image, (224, 224), interpolation=cv2.INTER_AREA)])
    activations = act_model.predict(x)
    for act_index in act_indexes:
        act = activations[act_index]
        indx = 0
        n_row = 8
        n_col = 8
        fig, ax = plt.subplots(n_row, n_col, figsize=(n_row*2.5, n_col*1.5))
        fig.suptitle(title)
        for row in range(n_row):
            for col in range(n_col):
                ax[row][col].imshow(act[0, :, :, indx], cmap='gray')
                indx += 1
        fig.savefig(os.path.join('viz', title + '_map' + str(act_index)), dpi=fig.dpi)
        plt.close(fig)


if __name__ == '__main__':
    start_time = time.time()
    # cifar10
    # cifar_label_decoder = {0: 'airplane', 1: 'automobile', 2: 'bird', 3: 'cat', 4: 'deer', 5: 'dog', 6: 'frog',
    #                        7: 'horse', 8: 'ship', 9: 'truck'}
    # (train_x, train_y), (test_x, test_y) = cifar10.load_data()
    # train_x = np.append(train_x, test_x, axis=0)
    # train_y = [y[0] for y in train_y] + [y[0] for y in test_y]
    # train_y = np.array(train_y)
    # train_y = [y[0] for y in train_y]
    # test_y = [y[0] for y in test_y]

    # memes data
    # X, Y = get_memes_data()
    # train_x = np.array(X)    # for memes

    # uuic events data (http://vision.stanford.edu/lijiali/event_dataset) 1579 images
    # X, Y = get_uuic_data()
    # train_x, test_x, train_y, test_y = train_test_split(X, Y, test_size=0.3)
    # train_x = np.array(train_x)
    # test_x = np.array(test_x)
    # train_y = np.array(train_y)
    # test_y = np.array(test_y)
    # train_x = np.array(X)
    # train_y = np.array(Y)
    # MNIST data
    (trainX, trainY), (testX, testY) = mnist.load_data()
    train_x = np.append(trainX, testX, axis=0)
    train_x = np.array([cv2.cvtColor(img, cv2.COLOR_GRAY2RGB) for img in train_x])
    train_y = np.append(trainY, testY)
    no_of_labels = 10
    print("training label distribution: ", Counter(train_y))
    # print("test label distribution: ", Counter(test_y))
    N = train_x.shape[0]
    print("Number of training data: " + str(N))

    # show random selection of images
    # rnd_idx = np.arange(N)
    # np.random.shuffle(rnd_idx)
    # plt_images = train_x[rnd_idx[0:16], :]
    # plt_images = train_x[rnd_idx[0:16]]
    # plot_images(4, 4, plt_images, 'Random images')

    # features, num_kps = get_sift_features(train_x, N)
    # get cnn model
    cnn_model = get_vgg_model()
    # cnn_model = get_resnet_model()
    n_regions = 50
    features, num_kps, image_patches = get_cnn_features(cnn_model, train_x, n_regions)
    print("shape of featureset: ", features.shape)

    # Now test data
    # N_test = test_x.shape[0]
    # print("number of test data: " + str(N_test))
    # total_desc_test, n_test_kps = get_sift_features(test_x, N_test)
    # total_desc_test, n_test_kps = get_cnn_features(cnn_model, test_x)
    # print('total descriptors: ' + str(total_desc_test.shape))

    # generating codebooks
    # number of visual words
    # for k in range(100, 501, 50):
    for k in [200]:
        print("number of visual words: ", k)
        kmeans = MiniBatchKMeans(n_clusters=k,
                                 init='k-means++',
                                 batch_size=500,
                                 random_state=0,
                                 verbose=0)
        kmeans.fit(features)
        # print(kmeans.get_params())
        # print(kmeans.cluster_centers_)
        k_labels = kmeans.labels_
        visual_word_clusters = get_cluster_from_labels(k_labels)
        k_centers = kmeans.cluster_centers_
        td_mat = vector_quantize(k, N, num_kps, k_labels)
        # print(td_mat.shape)
        # test_words = kmeans.predict(total_desc_test)
        # print(test_words)
        # histogram for test_data
        # td_mat_test = vector_quantize(k, N_test, n_test_kps, test_words)

        # train LDA
        # for n_topics in [4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24]:
        max_iterations = 100
        print('lda max_iterations: ', max_iterations)
        for n_topics in [no_of_labels]:
            print("number of topics: ", n_topics)
            m_predictions = []
            metrics = []
            topics_i = []    # topic clusters for ith iteration
            ith_topic_matrix = []
            for i in range(max_iterations):
                lda = LatentDirichletAllocation(n_components=n_topics, learning_method='online',  batch_size=512)
                lda.fit(td_mat.T)
                # print(lda.get_params())
                topic_matrix = lda.components_ / lda.components_.sum(axis=1)[:, np.newaxis]     # topics X terms
                ith_topic_matrix.append(topic_matrix)
                # print(topic_matrix.shape)
                # plt.matshow(topic_matrix, cmap='gray')
                # plt.title('topic matrix')
                # plt.ylabel('topics')
                # plt.xlabel('visual words')
                # plt.show()

                train_topic_matrix = lda.transform(td_mat.T)
                # test_topic_matrix = lda.transform(td_mat_test.T)
                # print("test topic matrix shape: ", test_topic_matrix.shape)

                topics_train, train_predictions = get_topics(train_topic_matrix)
                topics_i.append(topics_train)
                m_predictions.append(train_predictions)
                metrics.append((adjusted_rand_score(train_y, train_predictions),
                                normalized_mutual_info_score(train_y, train_predictions)))
                # topics_test, test_predictions = get_topics(test_topic_matrix)
                # for topic, indexes in topics_train.items():
                #     image_list = []
                #     for img_index, prob in indexes:
                #         image_list.append(train_x[img_index])
                #     # print('Topic #' + str(topic))
                #     # plot_images(4, 5, image_list[:20], 'Topic #' + str(topic))
                # for topic, indexes in topics_test.items():
                #     image_list = []
                #     for img_index, prob in indexes:
                #         image_list.append(test_x[img_index])
                #     # print('Topic #' + str(topic))
                #     # plot_images(4, 5, image_list[:20], 'Topic #' + str(topic))
            m_predictions = np.array(m_predictions)  # iteration * image
            n_predictions = np.transpose(m_predictions)   # image * iteration
            print(n_predictions.shape)
            n = n_predictions.shape[0]
            distance_matrix = np.zeros((n, n))
            # for each pair of images
            cache = {}
            for i, predi in enumerate(n_predictions):
                for j, predj in enumerate(n_predictions):
                    key = frozenset([i, j])
                    if cache.get(key):
                        distance_matrix[i][j] = cache.get(key)
                    else:
                        count = sum([ii == jj for ii, jj in zip(predi, predj)]) / max_iterations
                        # count the fraction of times they belong to the same topic
                        distance_matrix[i][j] = 1 - count   # dist = 1 - similarity
                        cache[key] = 1 - count

            print(distance_matrix)
            # for row in distance_matrix:
            #     print(row)
            clustering = AgglomerativeClustering(n_clusters=no_of_labels, affinity='precomputed', linkage='average')
            predictions = clustering.fit_predict(distance_matrix)
            print("Clustering Metrics")
            print(np.array(metrics)[:10])
            # predictions = train_predictions + test_predictions
            # predictions = train_predictions
            # truth = train_y + test_y
            truth = train_y
            ari = adjusted_rand_score(truth, predictions)
            nmi = normalized_mutual_info_score(truth, predictions)
            print("ARI: ", ari)
            print("NMI: ", nmi)

            final_clusters = get_cluster_from_labels(predictions)
            # print(ith_topic_matrix)
            cluster_def = {}
            # find the closest topic to clusters for interpretability
            for cluster_idx, img_indexes in final_clusters.items():
                max_count = -1
                i = -1
                topic_i = -1
                for iteration_number, topic_clusters in enumerate(topics_i):
                    # print('iteration: ', iteration_number)
                    for topic_idx, image_indexes_topic in topic_clusters.items():
                        # print('images in topic: ', image_indexes_topic)
                        count = sum([i in image_indexes_topic for i in img_indexes])
                        # print('count: ', count)
                        if count > max_count:
                            max_count = count
                            i = iteration_number
                            topic_i = topic_idx
                cluster_def[cluster_idx] = ith_topic_matrix[i][topic_i]

            cmat = np.zeros((n_topics, k))
            for c_index, defn in cluster_def.items():
                cmat[c_index] = defn
            plt.matshow(cmat, cmap='gray')
            plt.show()

            # getting top 10 words for a topic with their probabilities
            for t_idx, word_row in enumerate(cmat):
                print('topic: ' + str(t_idx))
                top10 = np.argsort(word_row)[-10:]
                cluster_imgs = [train_x[i] for i in final_clusters.get(t_idx)]
                plot_images(4, 4, cluster_imgs[:16], str(t_idx), save=True)
                for top_i in top10[::-1]:
                    print("word " + str(top_i) + ": " + str(word_row[top_i]))
                    word_patches = [image_patches[i] for i in visual_word_clusters.get(top_i)]
                    plot_images(4, 4, word_patches[:16], 'word #' + str(top_i), save=True)
                    visualize_cnn(cnn_model, word_patches[0], [1, 16], 'word #' + str(top_i))

            # use cosine similarity to get closest train image for test image
            # sim = cosine_similarity(test_topic_matrix, train_topic_matrix)
            #
            # nn = np.argmax(sim, axis=1)
            # nn5 = np.argsort(sim, axis=1)[:, -5:]
            # pred = []
            # pred5 = []
            # for k in range(N_test):
            #     nn_idx = nn[k]
            #     nn5_idx = nn5[k]
            #     pred.append(train_y[nn_idx])
            #     pred5.append([train_y[idx] for idx in nn5_idx])
            #
            # # Evaluation
            # print('acc: ' + str(accuracy_score(test_y, pred)))
            # print('precision: ' + str(precision_score(test_y, pred, average='weighted')))
            # print('recall: ' + str(recall_score(test_y, pred, average='weighted')))
            # print('confusion matrix: ')
            # print(confusion_matrix(test_y, pred))
            #
            # mapy = [[y] for y in test_y]
            # print('map5: ' + str(metrics.mapk(mapy, pred5, 5)))
            #
            # print('for random prediction')
            # rand_pred = np.random.randint(0, high=no_of_labels, size=N_test)
            # print('acc: ' + str(accuracy_score(test_y, rand_pred)))
            # print('precision: ' + str(precision_score(test_y, rand_pred, average='weighted')))
            # print('recall: ' + str(recall_score(test_y, rand_pred, average='weighted')))

            # # similar images
            # test_idx = np.arange(N_test)
            # np.random.shuffle(test_idx)
            # images = test_x[test_idx[0:16], :]
            # images_knn = train_x[nn[test_idx[0:16]], :]
            # plot_images(4, 4, images)
            # plot_images(4, 4, images_knn)
    print("Time elapsed in s: ", time.time() - start_time)
