import os
import gensim
import pandas as pd
import numpy as np
import time
from lda_text import tokenize
from lda_text import lematize
from sklearn.preprocessing import LabelEncoder
from collections import Counter
from ast import literal_eval
from gensim import models
from lda import get_topics
from sklearn.metrics import *
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.model_selection import StratifiedShuffleSplit
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sklearn.feature_extraction.text import TfidfVectorizer


# data = pd.read_csv(os.path.join('news', 'uci-news-aggregator.csv'))
# data = data[['TITLE', 'CATEGORY']]
# print(data['CATEGORY'].value_counts())
# data['TITLE'] = (data['TITLE'].map(tokenize)).map(lematize)
# print(data.head())
# data.to_csv('preprocessed_news.csv', index=False)

data = pd.read_csv(os.path.join('news', 'preprocessed_news.csv'))
print(data['CATEGORY'].value_counts())
print(data.head())
text_data = data['TITLE'].map(literal_eval)     # literal_eval evaluates the string to list
labels = data['CATEGORY']
label_encoder = LabelEncoder()
label_encoder.fit(labels)
labels = label_encoder.transform(labels)
print(label_encoder.classes_)
# print(label_encoder.inverse_transform(labels))
# data too big to perform in one pass so using Stratified shuffle split
sss = StratifiedShuffleSplit(n_splits=10, train_size=10000)
fold = 0
start_time = time.time()
for train_index, test_index in sss.split(text_data, labels):
    fold += 1
    print('For fold: ', str(fold))
    X_train, X_test = text_data[train_index], text_data[test_index]
    y_train, y_test = labels[train_index], labels[test_index]
    print(Counter(y_train))
    no_of_topics = 4
    print('tfidf')
    X = [" ".join(x) for x in X_train]
    tfidf = TfidfVectorizer(max_df=0.7, min_df=5, stop_words='english', use_idf=True)
    X = tfidf.fit_transform(X)
    km = KMeans(n_clusters=no_of_topics, init='k-means++', max_iter=100)
    km.fit(X)
    print("ARI: {}".format(adjusted_rand_score(y_train, km.labels_)))
    print("NMI: {}".format(normalized_mutual_info_score(y_train, km.labels_)))

    print('tf-idf + agglo')
    ag = AgglomerativeClustering(n_clusters=no_of_topics)
    ag_labels = ag.fit_predict(X.toarray())
    print("ARI: {}".format(adjusted_rand_score(y_train, ag_labels)))
    print("NMI: {}".format(normalized_mutual_info_score(y_train, ag_labels)))

    documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(X_train)]
    d2v = Doc2Vec(vector_size=5, window=1, min_count=1, workers=4)
    d2v.build_vocab(documents)
    d2v.train(documents, total_examples=d2v.corpus_count, epochs=100)
    print("----- doc2vec + K means -----")
    kmeans = KMeans(n_clusters=no_of_topics, init='k-means++', max_iter=100)
    kmeans.fit(d2v.docvecs.vectors_docs)
    k_labels = kmeans.labels_.tolist()
    print("ARI: ", adjusted_rand_score(y_train, k_labels))
    print("NMI: ", normalized_mutual_info_score(y_train, k_labels))
    print("----- doc2vec + agglo -----")
    agglo = AgglomerativeClustering(n_clusters=no_of_topics)
    agglo_labels = agglo.fit_predict(d2v.docvecs.vectors_docs)
    print("ARI: ", adjusted_rand_score(y_train, agglo_labels))
    print("NMI: ", normalized_mutual_info_score(y_train, agglo_labels))

    dictionary = gensim.corpora.Dictionary(X_train)
    document_frequency = dictionary.dfs
    # finding words that are the most frequent in all documents
    freq_words = sorted([(token, freq) for token, freq in document_frequency.items()], key=lambda tup: -1 * tup[1])
    # print("---- Top 10 Words with most number of occurrence in documents----")
    # id2token = {token_id: token for token, token_id in dictionary.token2id.items()}
    # for token_id, freq in freq_words[:10]:
    #     print("{} : {}, {}".format(id2token.get(token_id), freq, freq / dictionary.num_docs))

    # making corpus using dictionary
    corpus = [dictionary.doc2bow(d) for d in X_train]
    print("----- LDA -----")
    max_iterations = 100
    print('iterations for LDA: ', str(max_iterations))
    m_predictions = []
    metrics = []
    topics_i = []    # topic clusters for ith iteration
    ith_topic_term_matrix = []
    for i in range(max_iterations):
        lda = models.LdaModel(corpus, num_topics=no_of_topics, id2word=dictionary, alpha="auto", passes=20)
        topic_def_mat = ["" for k in range(no_of_topics)]
        for topic_i, defn in lda.show_topics(no_of_topics):
            topic_def_mat[topic_i] = defn
        ith_topic_term_matrix.append(topic_def_mat)
        topic_mat = np.zeros((len(corpus), no_of_topics))
        for doc_index, document in enumerate(corpus):
            topic_dist = lda.get_document_topics(document)
            for topic_index, prob in topic_dist:
                topic_mat[doc_index][topic_index] = prob
        # print(topic_mat)
        # print(topic_mat.shape)
        # print([lda.get_document_topics(d) for d in corpus[:5]])
        topics, preds = get_topics(topic_mat)
        topics_i.append(topics)
        m_predictions.append(preds)
        metrics.append((adjusted_rand_score(y_train, preds),
                        normalized_mutual_info_score(y_train, preds)))

    m_predictions = np.array(m_predictions)  # iteration * image
    n_predictions = np.transpose(m_predictions)   # image * iteration
    print(np.array(metrics)[:10])
    n = n_predictions.shape[0]
    distance_matrix = np.zeros((n, n))
    # for each pair of images
    cache = {}
    for i, predi in enumerate(n_predictions):
        for j, predj in enumerate(n_predictions):
            key = frozenset([i, j])
            if cache.get(key):
                distance_matrix[i, j] = cache.get(key)
            else:
                count = sum([ii == jj for ii, jj in zip(predi, predj)]) / max_iterations
                # count the fraction of times they belong to the same topic
                distance_matrix[i, j] = 1 - count   # dist = 1 - similarity
                cache[key] = 1 - count

    # print(distance_matrix)
    clustering = AgglomerativeClustering(n_clusters=no_of_topics, affinity='precomputed', linkage='average')
    predictions = clustering.fit_predict(distance_matrix)
    ari = adjusted_rand_score(y_train, predictions)
    nmi = normalized_mutual_info_score(y_train, predictions)
    print("ARI: ", ari)
    print("NMI: ", nmi)

    final_clusters = {}
    for idx, label in enumerate(predictions):
        if not final_clusters.get(label):
            final_clusters[label] = [idx]
        else:
            (final_clusters[label]).append(idx)

    cluster_def = {}
    # find the closest topic to clusters for interpretability
    for cluster_idx, img_indexes in final_clusters.items():
        max_count = -1
        i = -1
        topic_i = -1
        for iteration_number, topic_clusters in enumerate(topics_i):
            # logging.warn('iteration: ', iteration_number)
            for topic_idx, image_indexes_topic in topic_clusters.items():
                # logging.warn('images in topic: ', image_indexes_topic)
                count = sum([i in image_indexes_topic for i in img_indexes])
                # logging.warn('count: ', count)
                if count > max_count:
                    max_count = count
                    i = iteration_number
                    topic_i = topic_idx
        cluster_def[cluster_idx] = ith_topic_term_matrix[i][topic_i]

    for cluster_idx, cluster_defn in cluster_def.items():
        print("Topic: {}".format(cluster_idx))
        print(cluster_defn)
        text_indexes = final_clusters.get(cluster_idx)
        for text_index in text_indexes[:10]:
            print("- {} : {}".format(X_train.iloc[text_index], label_encoder.inverse_transform([y_train[text_index]])))

print("Time elapsed in s: ", time.time() - start_time)