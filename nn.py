"""Nearest neighbor approach to classify unlabeled data using LDA"""

import os
import numpy as np
import pandas as pd
from lda_text import TextLda
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC


# get all data
data_path = os.path.join('MemeSample', 'data.csv')
# perform all operations related to LDA of text
textLda = TextLda(data_path, encoding='cp1252', use_bigrams=True)
# getting random data to check the image and text
N = len(textLda.corpus)
rand_index = np.random.choice(range(N))
print(textLda.preprocessed_data[rand_index])
dictionary = textLda.dictionary
text_dictionary_size = len(dictionary)
text_feature_vector = np.zeros((N, text_dictionary_size))
# build a document-term matrix as feature vector
for doc_idx, doc in enumerate(textLda.corpus):
    for key, word_count in doc:
        text_feature_vector[doc_idx][key] = word_count
print(text_feature_vector.shape)

n_topics = 6

# perform LDA to get document topic matrix
lda_model = LatentDirichletAllocation(n_components=n_topics, learning_method='online')
lda_model.fit(text_feature_vector)
topic_term_matrix = lda_model.components_ / lda_model.components_.sum(axis=1)[:, np.newaxis]  # topics X terms
document_topic_matrix = lda_model.transform(text_feature_vector)
print(document_topic_matrix)

# get labeled data
labeled_data = pd.read_csv(os.path.join('MemeSample', 'immigration_meme.csv'))
labeled_data = labeled_data[['Id', 'themes']]
labeled_data.dropna(inplace=True)
# multiple entries for multiple themes
for i in range(len(labeled_data)):
    row = labeled_data.iloc[i]
    themes = row.themes.split(",")
    if len(themes) > 1:
        row.themes = themes[0]
        labeled_data.iloc[i] = row
        for j in range(1, len(themes)):
            new_row = labeled_data.iloc[i]
            new_row.themes = themes[j]
            labeled_data = labeled_data.append(new_row, ignore_index=False, sort=True)

labeled_data.themes = labeled_data['themes'].map(lambda x: x.split()[0].split('/')[0])
print(labeled_data.head())

# encode labels
label_encoder = LabelEncoder()
label_encoder.fit(labeled_data['themes'])
labels = label_encoder.transform(labeled_data['themes'])
labeled_data['label'] = labels
print(labeled_data.head())

# prepare features for labeled data to pass into classifier
X = []
Y = []
for i in range(len(labeled_data)):
    r = labeled_data.iloc[i]
    X.append(document_topic_matrix[int(r.Id) - 1])     # index = Id - 1 for all data
    Y.append(r.label)

X = np.array(X)
Y = np.array(Y)

sss = StratifiedShuffleSplit(n_splits=10, test_size=0.2)
knn_acc, svm_acc = [], []
for train_index, test_index in sss.split(X, labels):
    X_train, X_val = X[train_index], X[test_index]
    y_train, y_val = labels[train_index], labels[test_index]
    # standardize data
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_val = scaler.fit_transform(X_val)

    # knn
    knn = KNeighborsClassifier(n_neighbors=3)
    knn.fit(X_train, y_train)
    knn_acc.append(knn.score(X_val, y_val))

    # svm
    svm = SVC(gamma='auto')
    svm.fit(X_train, y_train)
    svm_acc.append(svm.score(X_val, y_val))

print('KNN')
print(np.mean(knn_acc))
print('SVM')
print(np.mean(svm_acc))
