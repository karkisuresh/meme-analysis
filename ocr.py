import cv2
import pytesseract
import glob
import os
import io
import numpy as np
from common import Sketcher
from google.cloud import vision
from sklearn.cluster import MiniBatchKMeans

# setting credentials
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = r'D:\College\Project\api\Meme analysis-1fc86ff33b21.json'

img_path = os.path.join('MemeSample', '*.png')

# test for results of pytesseract
# for image in glob.glob(img_path):
#     img = cv2.imread(image)
#     data = pytesseract.image_to_data(img, lang='eng', output_type=pytesseract.Output.DICT)
#     no_of_boxes = len(data['level'])
#     conf = data['conf']
#     print(conf)
#     font = cv2.FONT_HERSHEY_SIMPLEX
#     for i in range(no_of_boxes):
#         if conf[i] == '-1' or conf[i] < 25:
#             continue
#         (x, y, w, h) = (data['left'][i], data['top'][i], data['width'][i], data['height'][i])
#         cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
#     cv2.imshow('img', img)
#     cv2.waitKey()

# initialize google cloud client
client = vision.ImageAnnotatorClient()

for image_path in glob.glob(img_path):
    filename = image_path.split("\\")[1]
    if filename in os.listdir("memes"):
        print('Skipping {}'.format(filename))
        continue
    # read image
    with io.open(image_path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    # call vision api for text detection
    response = client.text_detection(image=image)

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))

    text_annotations = response.text_annotations
    img = cv2.imread(image_path)
    img_test = img.copy()
    gray = cv2.cvtColor(img_test, cv2.COLOR_BGR2GRAY)
    mask = np.zeros(img_test.shape[:2], np.uint8)
    whites = np.zeros(img_test.shape[:2], np.uint8)
    whites.fill(255)
    sketch = Sketcher('mask', [mask], lambda: ((255, 255, 255), 255))
    for ta in text_annotations[1:]:
        bounds = ta.bounding_poly.vertices
        start_point = bounds[0]
        end_point = bounds[2]
        x, y = start_point.x, start_point.y
        x_w, y_h = end_point.x, end_point.y
        # cv2.rectangle(img_test, start_point, end_point, (0, 255, 0), 2)
        mask[y:y_h, x:x_w] = whites[y:y_h, x:x_w]

    # cv2.imshow('img', img_test)
    # cv2.waitKey(0)
    # cv2.imshow("mask", mask)
    # cv2.waitKey(0)
    final = cv2.inpaint(img_test, mask, 3, cv2.INPAINT_TELEA)
    # cv2.imshow('final', final)
    # cv2.waitKey(0)
    cv2.imwrite(os.path.join("memes", filename), final)
    cv2.destroyAllWindows()

    # test the LDA
    # from lda import get_vgg_model, get_cnn_features, get_cluster_from_labels, plot_images
    # for img in [img, final]:
    #     cnn_model = get_vgg_model()
    #     n_regions = 50
    #     features, num_kps, image_patches = get_cnn_features(cnn_model, np.array([img]), n_regions)
    #     k = 5
    #     print("number of visual words: ", k)
    #     kmeans = MiniBatchKMeans(n_clusters=k,
    #                              init='k-means++',
    #                              batch_size=500,
    #                              random_state=0,
    #                              verbose=0)
    #     kmeans.fit(features)
    #     # print(kmeans.get_params())
    #     # print(kmeans.cluster_centers_)
    #     k_labels = kmeans.labels_
    #     visual_word_clusters = get_cluster_from_labels(k_labels)
    #     print(visual_word_clusters)
    #     for idx, images in visual_word_clusters.items():
    #         plot_images(4, 4, [image_patches[i] for i in images][:16], str(idx))





