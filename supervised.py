import pandas as pd
import os
import gensim
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from sklearn.metrics import f1_score, accuracy_score
from lda_text import tokenize
from lda_text import lematize
from collections import Counter
from gensim import models
from sklearn.preprocessing import StandardScaler

# read the immigration data without text
data = pd.read_csv(os.path.join('MemeSample', 'immigration_meme.csv'))
# read the text data
text_data = pd.read_csv(os.path.join('MemeSample', 'data.csv'), encoding='cp1252')
data = data[['Id', 'label', 'themes']]
data = pd.merge(text_data, data, on='Id')
# dropping nan values
data.dropna(inplace=True)
# for multiple themes create multiple inserts
for i in range(len(data)):
    row = data.iloc[i]
    themes = row.themes.split(",")
    if len(themes) > 1:
        row.themes = themes[0]
        data.iloc[i] = row
        for j in range(1, len(themes)):
            new_row = data.iloc[i]
            new_row.themes = themes[j]
            data = data.append(new_row, ignore_index=False, sort=True)

data.themes = data['themes'].map(lambda x: x.split()[0].split('/')[0])

print('Number of samples: ', len(data))

# print samples
sample = data.iloc[np.random.randint(0, len(data))]
print('text:', sample.Text)
print('tone:', sample.label)
print('theme:', sample.themes)

labels = data['themes']
# plot distribution of classes
# label_counter = Counter(labels)
# plt.bar(np.arange(6), label_counter.values())
# plt.xticks(np.arange(6), label_counter.keys())
# plt.xlabel('class labels')
# plt.ylabel('number of samples')
# plt.title('number of samples per class labels')
# plt.show()
label_encoder = LabelEncoder()
label_encoder.fit(labels)
labels = label_encoder.transform(labels)
no_of_labels = 6

# preprocess the text
text = (data['Text'].map(tokenize)).map(lematize)
# add bigrams
phrases = models.Phrases(text, min_count=3, scoring='npmi', threshold=0)
bigram = models.phrases.Phraser(phrases)
preprocessed_text = []
for t in text:
    preprocessed_text.append(bigram[t])

# create dictionary and corpus
dictionary = gensim.corpora.Dictionary(preprocessed_text)
dictionary.filter_extremes(no_below=2, no_above=0.8)
corpus = np.array([dictionary.doc2bow(d) for d in preprocessed_text])

for no_of_topics in [6]:
    print('number of topics:', no_of_topics)
    lr_metrics, sgd_metrics, svm_metrics = [], [], []
    sss = StratifiedShuffleSplit(n_splits=10, test_size=0.2)
    for train_index, test_index in sss.split(corpus, labels):
        X_train, X_test = corpus[train_index], corpus[test_index]
        print('training set size: ', len(X_train))
        print('test set size: ', len(X_test))
        y_train, y_test = labels[train_index], labels[test_index]
        lda_train = models.LdaModel(X_train, num_topics=no_of_topics, id2word=dictionary, alpha="auto")
        features_train = np.zeros((len(X_train), no_of_topics))
        features_test = np.zeros((len(X_test), no_of_topics))
        # construct feature vector for training data
        for i, bow in enumerate(X_train):
            topic_vec = np.zeros(no_of_topics)
            for topic_id, prob in lda_train.get_document_topics(bow):
                topic_vec[topic_id] = prob
            features_train[i] = topic_vec
        # construct feature vector for test data
        for i, bow in enumerate(X_test):
            topic_vec = np.zeros(no_of_topics)
            for topic_id, prob in lda_train.get_document_topics(bow):
                topic_vec[topic_id] = prob
            features_test[i] = topic_vec

        # standardize data
        scaler = StandardScaler()
        features_train_scaled = scaler.fit_transform(features_train)
        features_test_scaled = scaler.fit_transform(features_test)

        # logistic regression
        lr = LogisticRegression(solver='newton-cg', multi_class='multinomial')
        lr.fit(features_train_scaled, y_train)
        y_pred = lr.predict(features_test_scaled)
        f1 = f1_score(y_test, y_pred, average='weighted')
        acc = accuracy_score(y_test, y_pred)
        lr_metrics.append((acc, f1))

        # Logistic Regression SGD
        sgd = SGDClassifier(max_iter=1000, loss='log')
        sgd.fit(features_train_scaled, y_train)
        y_pred = sgd.predict(features_test_scaled)
        f1 = f1_score(y_test, y_pred, average='weighted')
        acc = accuracy_score(y_test, y_pred)
        sgd_metrics.append((acc, f1))

        # SVM
        svm = SVC(gamma='auto')
        svm.fit(features_train_scaled, y_train)
        y_pred = svm.predict(features_test_scaled)
        f1 = f1_score(y_test, y_pred, average='weighted')
        acc = accuracy_score(y_test, y_pred)
        svm_metrics.append((acc, f1))

    # get avg of metrics
    print('logistic regression')
    print('acc:', np.mean([acc for acc, f1 in lr_metrics]))
    print('f1:', np.mean([f1 for acc, f1 in lr_metrics]))

    print('logistic regression sgd')
    print('acc:', np.mean([acc for acc, f1 in sgd_metrics]))
    print('f1:', np.mean([f1 for acc, f1 in sgd_metrics]))

    print('SVM')
    print('acc:', np.mean([acc for acc, f1 in svm_metrics]))
    print('f1:', np.mean([f1 for acc, f1 in svm_metrics]))
