import pandas as pd
import os
import matplotlib.pyplot as plt
import cv2
import gensim
import numpy as np
import networkx as nx
from networkx.algorithms import bipartite
from gensim import models
from gensim.utils import simple_preprocess
from gensim.matutils import hellinger, jaccard
from gensim.parsing.preprocessing import STOPWORDS
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from itertools import combinations
from lda import get_topics
from sklearn.cluster import AgglomerativeClustering
from collections import OrderedDict
from sklearn.preprocessing import LabelEncoder
from sklearn import metrics


import nltk
# nltk.download('wordnet')

import warnings
warnings.filterwarnings('ignore')


def get_pos(word):
    word, tag = nltk.pos_tag([word])[0]
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag[0], wordnet.NOUN)


def tokenize(text):
    return simple_preprocess(text, deacc=True)


def lematize(doc):
    pre_doc = []
    for token in doc:
        lem_token = WordNetLemmatizer().lemmatize(token, get_pos(token))
        if lem_token not in STOPWORDS and len(lem_token) > 2:
            pre_doc.append(lem_token)
    return pre_doc


class TextLda:

    def __init__(self, path, encoding='utf-8', use_bigrams=False):
        self.data = pd.read_csv(path, encoding=encoding)
        self.remove_other_category()
        print(self.data.head())
        self.preprocessed_data, self.dictionary = self.create_dictionary(use_bigrams=use_bigrams)
        self.corpus = [self.dictionary.doc2bow(d) for d in self.preprocessed_data]
        self.model = None
        self.bigram_model = None

    def remove_other_category(self):
        other_id = [26, 73, 334, 178, 180, 275, 342, 95, 128]
        for other in other_id:
            self.data = self.data[self.data.Id != other]

    def get_random_data(self):
        random_data = self.data.sample().iloc[0]
        print(random_data.Text)
        img_path = os.path.join('MemeSample', str(random_data.Id) + '.png')
        img = cv2.imread(img_path)
        plt.imshow(img)
        plt.show()
        return random_data

    def print_score(self, pred_labels):
        labeled_data = pd.read_csv(os.path.join('MemeSample', 'immigration_meme.csv'))
        labeled_data = labeled_data[['Id', 'themes']]
        labeled_data.dropna(inplace=True)
        label_encoder = LabelEncoder()
        label_encoder.fit(labeled_data[['themes']])
        truths = label_encoder.transform(labeled_data[['themes']])
        y = []
        for li in range(len(labeled_data)):
            row = labeled_data.iloc[li]
            y.append(pred_labels[int(row.Id) - 1])     # index = id - 1
        print('nmi:', metrics.normalized_mutual_info_score(truths, y))
        print('ari:', metrics.adjusted_rand_score(truths, y))

    def init_bigram_model(self, data):
        bigram = models.Phrases(data, min_count=3, scoring='npmi', threshold=0)  # threshold between -1 and 1.
        # faster model used when we don't to add any more docs
        self.bigram_model = models.phrases.Phraser(bigram)
        # print(self.bigram_model[data[64]])

    def add_bigrams(self, text):
        return self.bigram_model[text]

    def create_dictionary(self, use_bigrams=False):
        preprocessed_data = self.data['Text'].map(tokenize)
        preprocessed_data = preprocessed_data.map(lematize)
        if use_bigrams:
            self.init_bigram_model(preprocessed_data)
            preprocessed_data = preprocessed_data.map(self.add_bigrams)
        # print(preprocessed_data[:10])
        dictionary = gensim.corpora.Dictionary(preprocessed_data)
        # filter out words that are in less than 2 documents and that are in more than 80% of documents
        dictionary.filter_extremes(no_below=2, no_above=0.8)
        dictionary.filter_n_most_frequent(4)
        # print(dictionary)
        document_frequency = dictionary.dfs
        # finding words that are the most frequent in all documents
        freq_words = sorted([(token, freq) for token, freq in document_frequency.items()], key=lambda tup: -1 * tup[1])
        # print("---- Top 10 Words with most number of occurrence in documents----")
        # id2token = {token_id: token for token, token_id in dictionary.token2id.items()}
        # for token_id, freq in freq_words[:10]:
        #     print("{} : {}, {}".format(id2token.get(token_id), freq, freq / dictionary.num_docs))
        return preprocessed_data, dictionary

    def perform_lda(self, no_of_topics):
        lda = models.LdaModel(self.corpus, num_topics=no_of_topics, id2word=self.dictionary, alpha="auto", passes=20)
        return lda

    def set_model(self, model):
        self.model = model

    def find_n_topics(self):
        prev_n = 4
        prev_lda = self.perform_lda(prev_n)
        for n_topics in range(5, 31):
            new_lda = self.perform_lda(n_topics)
            first_nodes = range(prev_n)
            second_nodes = [str(n) for n in range(n_topics)]
            assigment = {}
            match = True
            for i2 in range(n_topics):
                min_dist = 1.5
                min_index = 100
                for i1 in range(prev_n):
                    # Hellinger distance is used to quantify the similarity between two probability distributions.
                    distance = hellinger(prev_lda.get_topics()[i1], new_lda.get_topics()[i2])
                    if distance < min_dist:
                        min_dist = distance
                        min_index = i1
                if assigment.get(first_nodes[min_index]):
                    assigment.get(first_nodes[min_index]).append(second_nodes[i2])
                else:
                    assigment[first_nodes[min_index]] = [second_nodes[i2]]

            print(assigment)

            for k in range(prev_n):
                if not assigment.get(k):
                    match = False

            if not match:
                print("no match")
                graph = nx.Graph()
                graph.add_nodes_from(first_nodes, bipartite=0)
                graph.add_nodes_from(second_nodes, bipartite=1)
                for n1 in first_nodes:
                    if assigment.get(n1):
                        for n2 in assigment.get(n1):
                            graph.add_edge(n1, n2)
                pos = nx.bipartite_layout(graph, range(prev_n))
                nx.draw_networkx_nodes(graph, pos)
                nx.draw_networkx_edges(graph, pos)
                plt.show()
                return prev_n, prev_lda
            prev_n = n_topics
            prev_lda = new_lda
        return prev_n, prev_lda

    def print_top_words_for_topic(self):
        # initialize clusters
        num_topics = self.model.get_topics().shape[0]
        clusters = {}
        for k in range(num_topics):
            clusters[k] = []

        for index, bow in enumerate(self.corpus):
            document_topic = self.model.get_document_topics(bow)
            # print(document_topic)
            topic_index, prob = sorted(document_topic, key=lambda tup: -1 * tup[1])[0]
            # print(topic, prob)
            (clusters[topic_index]).append((index, prob))

        print("Topic distribution:")
        counter = [len(docs) for k, docs in clusters.items()]
        print(counter)

        for topic_index, text_prob in clusters.items():
            print("Topic: ", topic_index)
            for text_idx, prob in sorted(text_prob, key=lambda tup: -1 * tup[1])[:10]:
                print("{}".format(self.data.iloc[text_idx].Text))

    def find_k_using_coherence(self):
        results = []
        for num_topics in range(2, 27, 4):
            lda = self.perform_lda(num_topics)
            perplexity = lda.log_perplexity(self.corpus)
            lda_coherence_model = models.CoherenceModel(model=lda, texts=self.preprocessed_data,
                                                        dictionary=self.dictionary, coherence='c_v')
            lda_coherence = lda_coherence_model.get_coherence()
            results.append((num_topics, perplexity, lda_coherence))

        x = [n for n, p, c in results]
        y = [c for n, p, c in results]
        best_index = np.argmax(y)
        plt.plot(x, y)
        plt.xlabel('number of topics')
        plt.ylabel('topic coherence')
        plt.show()
        return results[best_index]

    def evaluate_stability(self, k, f, iterations=100, ):
        distributions = []
        print("!{} {}".format(k, iterations))
        f.write("!{} {}\n".format(k, iterations))
        for ith in range(iterations):
            lda = self.perform_lda(k)
            # for idx, topic in lda.print_topics(-1):
            #     print('Topic: ', idx)
            #     print(topic)
            for indx, topic in lda.show_topics(formatted=False):
                topic_words = " ".join([word for word, prob in topic])
                print("{} {}".format(indx, topic_words))
                f.write("{} {}\n".format(indx, topic_words))
            distributions.append([word for n in range(k) for word, prob in lda.show_topic(n)])
            f.flush()
        # intersection = set(distributions[0]) & set(distributions[1])
        # union = set(distributions[0]) | set(distributions[1])
        # print("all words:", union)
        # print("Uncommon words:", union - intersection)
        # print("distance", jaccard(distributions[0], distributions[1]))
        # distances = []
        # for d1, d2 in combinations(distributions, 2):
        #     if d1 != d2:
        #         dist = jaccard(d1, d2)
        #         distances.append(dist)
        # similarity = 1 - sum(distances)/len(distances)
        # print("avg % of common words over " + str(iterations) + " runs:", similarity)
        # return similarity
        return 1


if __name__ == "__main__":
    data_path = os.path.join('MemeSample', 'data.csv')
    textLda = TextLda(data_path, encoding='cp1252', use_bigrams=True)
    random_value = textLda.get_random_data()
    print(lematize(tokenize(random_value.Text)))
    # best_result = textLda.find_k_using_coherence()
    # print(best_result)
    # num_of_topics, lda_model = textLda.find_n_topics()
    num_of_topics = 6
    # run multiple iterations of the LDA and use agglomerative clustering to cluster the data
    max_iterations = 100
    print('iterations for LDA: ', str(max_iterations))
    m_predictions = []
    topics_i = []  # topic clusters for ith iteration
    ith_topic_def_matrix = []
    for i in range(max_iterations):
        lda_model = textLda.perform_lda(num_of_topics)
        topic_def_mat = ["" for k in range(num_of_topics)]
        for topic_i, defn in lda_model.show_topics(num_of_topics):
            topic_def_mat[topic_i] = defn
        ith_topic_def_matrix.append(topic_def_mat)
        topic_mat = np.zeros((len(textLda.corpus), num_of_topics))
        for doc_index, document in enumerate(textLda.corpus):
            topic_dist = lda_model.get_document_topics(document)
            for topic_indx, probability in topic_dist:
                topic_mat[doc_index][topic_indx] = probability
        topics, preds = get_topics(topic_mat)
        topics_i.append(topics)
        m_predictions.append(preds)

    m_predictions = np.array(m_predictions)  # iteration * image
    n_predictions = np.transpose(m_predictions)  # image * iteration
    n_size = n_predictions.shape[0]
    distance_matrix = np.zeros((n_size, n_size))
    # for each pair of images
    cache = {}
    for i, predi in enumerate(n_predictions):
        for j, predj in enumerate(n_predictions):
            key = frozenset([i, j])
            if cache.get(key):
                distance_matrix[i, j] = cache.get(key)
            else:
                count = sum([ii == jj for ii, jj in zip(predi, predj)]) / max_iterations
                # count the fraction of times they belong to the same topic
                distance_matrix[i, j] = 1 - count  # dist = 1 - similarity
                cache[key] = 1 - count

    clustering = AgglomerativeClustering(n_clusters=num_of_topics, affinity='precomputed', linkage='average')
    labels = clustering.fit_predict(distance_matrix)
    textLda.print_score(labels)
    final_clusters = {}
    for idx, label in enumerate(labels):
        if not final_clusters.get(label):
            final_clusters[label] = [idx]
        else:
            (final_clusters[label]).append(idx)

    cluster_def = {}
    # find the closest topic to clusters for interpretability
    for cluster_idx, img_indexes in final_clusters.items():
        max_count = -1
        i = -1
        topic_i = -1
        for iteration_number, topic_clusters in enumerate(topics_i):
            # print('iteration: ', iteration_number)
            for topic_idx, image_indexes_topic in topic_clusters.items():
                # print('images in topic: ', image_indexes_topic)
                count = sum([i in image_indexes_topic for i in img_indexes])
                # print('count: ', count)
                if count > max_count:
                    max_count = count
                    i = iteration_number
                    topic_i = topic_idx
        cluster_def[cluster_idx] = ith_topic_def_matrix[i][topic_i]

    for cluster_idx, cluster_defn in cluster_def.items():
        print("Topic: ", cluster_idx)
        print(cluster_defn)
        text_indexes = final_clusters.get(cluster_idx)
        for text_index in text_indexes[:15]:
            print("- {}".format(textLda.data.iloc[text_index].Text))

    # lda_model = textLda.perform_lda(num_of_topics)
    # for idx, topic in lda_model.print_topics(-1):
    #     print('Topic: ', idx)
    #     print(topic)
    # textLda.set_model(lda_model)
    # textLda.print_top_words_for_topic()
    # coherence_model = models.CoherenceModel(model=lda_model, texts=textLda.preprocessed_data,
    #                                         dictionary=textLda.dictionary, coherence='c_v')
    # coherence = coherence_model.get_coherence()
    # print('------------Metrics-----------')
    # print("coherence: ", coherence)
    # stability = []
    # for k in range(2, 40, 4):
    # with open("topics.txt", "w+") as file:
    #     for no_topics in [6, 14]:
    #         stability.append(textLda.evaluate_stability(no_topics, f=file))
    # plt.plot(range(2, 40, 4), stability)
    # plt.xlabel("number of topics")
    # plt.ylabel("avg % of common words over 50 runs")
    # plt.show()
