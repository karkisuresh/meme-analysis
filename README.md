# Meme analysis

- The original memes are under MemeSample directory where filename corresponds to the id
- MemeSample/data.csv contains the texts associated with memes along with ids
- MemeSample/immigration_meme.csv contains the labeled memes
- memes directory contains the images after removal of text
- event_img directory contains uuic events data (http://vision.stanford.edu/lijiali/event_dataset) 1579 images
- news directory contains uci-news-aggregator dataset
- lda.py contains program for visual LDA
- lda_text.py contains program for textual LDA
- mixLDA.py contains program for using combination of visual and textual features for LDA
	- Please create a directory named "viz" where the images of result is stored such as final clusters and visual words.
- news.py contains code for our approach of clustering using LDA for textual news-aggregator dataset
- news20.py contains code for our approach of clustering using LDA for textual 20news group dataset
	- Please create a directory named "results" such that the results of this run is stored inside the directory
- nn.py contains code where we try to use nearest neighbour to label the memes.
- ocr.py contains program for removal of text from memes (Please don't run this again as it has already been done)
- stability.py contains code for evaluating the stability of our model by creating histogram of range
- supervised.py contains code for our approach of using LDA for supervised learning 
- common.py is a utility program sourced from https://github.com/opencv/opencv/blob/master/samples/python/common.py 
which is used by our ocr program.