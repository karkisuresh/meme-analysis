import numpy as np
import pandas as pd
import gensim
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_20newsgroups
from collections import Counter
from news20 import preprocess
from lda import get_topics
from scipy.stats import variation


def evaluate_similiarity(distance1, distance2):
    print('d1')
    print("size:", len(distance1))
    print("mean:", np.mean(np.array(distance1)))
    print("std:", np.std(np.array(distance1)))
    print('d2')
    print("size:", len(distance2))
    print("mean:", np.mean(np.array(distance2)))
    print("std:", np.std(np.array(distance2)))
    
    # d1_d2 = list(zip(d1, d2))
    # df = pd.DataFrame(d1_d2, columns=['D1', 'D2'])
    # df.to_csv('similarity.csv', index=False)


categories = ['rec.sport.hockey', 'soc.religion.christian']
dataset = fetch_20newsgroups(subset='all', shuffle=True, remove=('headers', 'footers', 'quotes'), categories=categories)
X = dataset.data
Y = dataset.target
print("%d documents" % len(X))
print("categories: ", dataset.target_names)
no_of_clusters = np.unique(Y).shape[0]
print(Counter(Y))
X = preprocess(X)
dictionary = gensim.corpora.Dictionary(X)
corpus = [dictionary.doc2bow(d) for d in X]
iterations = 10    # number of iterations to run the stability experiment
print('iterations: ', iterations)
for m in [100]:      # number of lda iterations
    print('m:', m)
    distance_matrices = []
    for k in range(iterations):
        m_predictions = []
        for i in range(m):
            lda = gensim.models.LdaModel(corpus, num_topics=no_of_clusters, id2word=dictionary, alpha="auto", passes=20)
            topic_mat = np.zeros((len(corpus), no_of_clusters))
            for doc_index, document in enumerate(corpus):
                topic_dist = lda.get_document_topics(document)
                for topic_indx, probability in topic_dist:
                    topic_mat[doc_index][topic_indx] = probability
            topics, preds = get_topics(topic_mat)
            m_predictions.append(preds)
        m_predictions = np.array(m_predictions)  # iteration * image
        n_predictions = np.transpose(m_predictions)  # image * iteration
        n = n_predictions.shape[0]
        distance_matrix = np.zeros((n, n))
        # for each pair of images
        d1 = []     # set of distance for every pair of documents that belong to the same cluster
        d2 = []     # set of distance for every pair of documents that belong to different cluster
        cache = {}
        for i, predi in enumerate(n_predictions):
            for j, predj in enumerate(n_predictions):
                key = frozenset([i, j])
                if cache.get(key):
                    distance_matrix[i, j] = cache.get(key)
                else:
                    # count = sum([ii == jj for ii, jj in zip(predi, predj)]) / m
                    count = sum([ii == jj for ii, jj in zip(predi, predj)])
                    # count the fraction of times they belong to the same topic
                    distance_matrix[i, j] = m - count  # dist = 1 - similarity
                    cache[key] = m - count
                if i != j:  # diff docs
                    if Y[i] == Y[j]:    # same cluster
                        d1.append(distance_matrix[i, j])
                    else:
                        d2.append(distance_matrix[i, j])
        # evaluate_similiarity(d1, d2)
        print(distance_matrix)
        distance_matrices.append(distance_matrix)
    mini = np.min(distance_matrices, axis=0)
    maxi = np.max(distance_matrices, axis=0)
    dist_range = maxi - mini
    print("minimun: ", mini)
    print("maximum: ", maxi)
    print(dist_range)
    np.save('range_hist.npy', dist_range)
    plt.hist(np.ravel(dist_range), bins=range(0, 31, 1))
    plt.title('histogram of range of distance between pairs of documents')
    plt.xlabel('Range')
    plt.ylabel('# of pairs')
    plt.show()
    # print("mean: ", np.mean(distance_matrices, axis=0))
    # print("Std:", np.std(distance_matrices, axis=0))
    # var = variation(distance_matrices, axis=0)
    # var = np.nan_to_num(var)
    # print("CV:", var)
    # print('min CV:', np.min(var))
    # print('max CV:', np.max(var))
    # print("avg CV:", np.mean(var))
    # more_var = var[var >= 1]
    # print(more_var)
    # print(len(more_var))
