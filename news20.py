import numpy as np
import pandas as pd
import gensim
import logging
import os
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn import metrics
from collections import Counter
from time import time
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim import models
from lda_text import tokenize
from lda_text import lematize
from lda import get_topics
from datetime import date


def preprocess(docs):
    result = []
    for d in docs:
        d = tokenize(d)
        result.append(lematize(d))

    # adding bigrams
    bigram = models.Phrases(result, min_count=5, scoring='npmi', threshold=0)  # threshold between -1 and 1.
    bigram_model = models.phrases.Phraser(bigram)
    for ri, xi in enumerate(result):
        result[ri] = bigram_model[xi]
    return np.array(result)


def d2v(docs):
    documents = [TaggedDocument(doc, [ix]) for ix, doc in enumerate(docs)]
    d2v = Doc2Vec(vector_size=8, window=1, min_count=1, workers=4)
    d2v.build_vocab(documents)
    d2v.train(documents, total_examples=d2v.corpus_count, epochs=100)
    return d2v


if __name__ == '__main__':
    # initialize logging
    log_filename = os.path.join('results', os.path.basename(__file__) + str(date.today()))
    handlers = [logging.FileHandler(log_filename, mode='w+'), logging.StreamHandler()]
    logging.basicConfig(level=logging.WARN, format='%(message)s', handlers=handlers)

    # download data
    categories = ['rec.sport.hockey', 'soc.religion.christian']
    dataset = fetch_20newsgroups(subset='all', shuffle=True, remove=('headers', 'footers', 'quotes'))
    data = dataset.data
    # result file to store result
    logging.warn("%d documents" % len(data))
    logging.warn("%d categories" % len(dataset.target_names))
    logging.warn(dataset.target_names)
    Y = dataset.target
    no_of_clusters = np.unique(Y).shape[0]
    logging.warn('no_of_clusters: {}'.format(no_of_clusters))

    logging.warn(np.array(data[:5]))
    pre_data = preprocess(data)
    logging.warn(pre_data[:5])

    # logging.warn('simple tfidf vector and kmeans')
    vectorizer = TfidfVectorizer(max_df=0.8, min_df=5, stop_words='english', use_idf=True)
    X = vectorizer.fit_transform(data)

    logging.warn('tf-idf + kmeans')
    km = KMeans(n_clusters=no_of_clusters, init='k-means++', max_iter=100)
    km.fit(X)
    logging.warn("ARI: {}".format(metrics.adjusted_rand_score(Y, km.labels_)))
    logging.warn("NMI: {}".format(metrics.normalized_mutual_info_score(Y, km.labels_)))

    logging.warn('tf-idf + agglo')
    ag = AgglomerativeClustering(n_clusters=no_of_clusters)
    ag_labels = ag.fit_predict(X.toarray())
    logging.warn("ARI: {}".format(metrics.adjusted_rand_score(Y, ag_labels)))
    logging.warn("NMI: {}".format(metrics.normalized_mutual_info_score(Y, ag_labels)))

    logging.warn('doc2vec + k-means')
    doc2vec = d2v(pre_data)
    k_means = KMeans(n_clusters=no_of_clusters, init='k-means++', max_iter=100)
    k_means.fit(doc2vec.docvecs.vectors_docs)
    k_labels = k_means.labels_.tolist()
    logging.warn("ARI: {}".format(metrics.adjusted_rand_score(Y, k_labels)))
    logging.warn("NMI: {}".format(metrics.normalized_mutual_info_score(Y, k_labels)))
    logging.warn("----- doc2vec + agglo -----")
    agglo = AgglomerativeClustering(n_clusters=no_of_clusters)
    agglo_labels = agglo.fit_predict(doc2vec.docvecs.vectors_docs)
    logging.warn("ARI: {}".format(metrics.adjusted_rand_score(Y, agglo_labels)))
    logging.warn("NMI: {}".format(metrics.normalized_mutual_info_score(Y, agglo_labels)))

    dictionary = gensim.corpora.Dictionary(pre_data)
    print(dictionary)
    dictionary.filter_extremes(no_below=5, no_above=0.7)
    print(dictionary)
    corpus = [dictionary.doc2bow(d) for d in pre_data]
    logging.warn('single - LDA')
    lda_model = models.LdaModel(corpus, num_topics=no_of_clusters, id2word=dictionary, alpha="auto", passes=30)
    topic_mat = np.zeros((len(corpus), no_of_clusters))
    for doc_index, document in enumerate(corpus):
        topic_dist = lda_model.get_document_topics(document)
        for topic_indx, probability in topic_dist:
            topic_mat[doc_index][topic_indx] = probability
    topics, preds = get_topics(topic_mat)
    logging.warn("ARI: {} ".format(metrics.adjusted_rand_score(Y, preds)))
    logging.warn("NMI: {}".format(metrics.normalized_mutual_info_score(Y, preds)))

    start_time = time()
    logging.warn("----- LDA -----")
    m = 100
    m_predictions = []
    topics_i = []  # topic clusters for ith iteration
    ith_topic_def_matrix = []
    mets = []
    for i in range(m):
        lda = models.LdaMulticore(corpus, num_topics=no_of_clusters, id2word=dictionary, passes=30, workers=3,
                                  minimum_probability=0.0)
        topic_def_mat = ["" for k in range(no_of_clusters)]
        for topic_i, defn in lda.show_topics(no_of_clusters):
            topic_def_mat[topic_i] = defn
        ith_topic_def_matrix.append(topic_def_mat)
        topic_mat = np.zeros((len(corpus), no_of_clusters))
        for doc_index, document in enumerate(corpus):
            topic_dist = lda.get_document_topics(document)
            for topic_indx, probability in topic_dist:
                topic_mat[doc_index][topic_indx] = probability
        if (i + 1) % 10 == 0:
            print('iteration:', i+1)
        topics, preds = get_topics(topic_mat)
        topics_i.append(topics)
        m_predictions.append(preds)
        mets.append((metrics.adjusted_rand_score(Y, preds), metrics.normalized_mutual_info_score(Y, preds)))

    logging.warn(np.array(mets)[:10])
    m_predictions = np.array(m_predictions)  # iteration * image
    n_predictions = np.transpose(m_predictions)  # image * iteration
    n_size = n_predictions.shape[0]
    distance_matrix = np.zeros((n_size, n_size))
    # for each pair of images
    cache = {}
    for i, predi in enumerate(n_predictions):
        for j, predj in enumerate(n_predictions):
            key = frozenset([i, j])
            if cache.get(key):
                distance_matrix[i, j] = cache.get(key)
            else:
                count = sum([ii == jj for ii, jj in zip(predi, predj)]) / m
                # count the fraction of times they belong to the same topic
                distance_matrix[i, j] = 1 - count  # dist = 1 - similarity
                cache[key] = 1 - count

    logging.warn(distance_matrix)

    clustering = AgglomerativeClustering(n_clusters=no_of_clusters, affinity='precomputed', linkage='average')
    labels = clustering.fit_predict(distance_matrix)
    ari = metrics.adjusted_rand_score(Y, labels)
    nmi = metrics.normalized_mutual_info_score(Y, labels)
    logging.warn("ARI: {}".format(ari))
    logging.warn("NMI: {}".format(nmi))

    logging.warn('time: {}'.format(time() - start_time))

    final_clusters = {}
    for idx, label in enumerate(labels):
        if not final_clusters.get(label):
            final_clusters[label] = [idx]
        else:
            (final_clusters[label]).append(idx)

    cluster_def = {}
    # find the closest topic to clusters for interpretability
    for cluster_idx, img_indexes in final_clusters.items():
        max_count = -1
        i = -1
        topic_i = -1
        for iteration_number, topic_clusters in enumerate(topics_i):
            # logging.warn('iteration: ', iteration_number)
            for topic_idx, image_indexes_topic in topic_clusters.items():
                # logging.warn('images in topic: ', image_indexes_topic)
                count = sum([i in image_indexes_topic for i in img_indexes])
                # logging.warn('count: ', count)
                if count > max_count:
                    max_count = count
                    i = iteration_number
                    topic_i = topic_idx
        cluster_def[cluster_idx] = ith_topic_def_matrix[i][topic_i]

    for cluster_idx, cluster_defn in cluster_def.items():
        logging.warn("Topic: {}".format(cluster_idx))
        logging.warn(cluster_defn)
        text_indexes = final_clusters.get(cluster_idx)
        for text_index in text_indexes[:5]:
            logging.warn("- {} : {}".format(data[text_index], Y[text_index]))

